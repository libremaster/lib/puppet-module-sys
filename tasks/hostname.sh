#!/bin/sh

if [ -z "$PT_fqdn" ]; then
  echo "Need to pass FQDN"
  exit 1
fi

echo "$PT_fqdn"

/usr/bin/hostnamectl set-hostname $PT_fqdn
