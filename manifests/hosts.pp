# Class: configuration des hosts
class sys::hosts (
  String $localip = '127.0.1.1',
) {

  host {
    'localhost':
      ip           => '127.0.0.1',
      host_aliases => [];
    'ip6-localhost':
      ip           => '::1',
      host_aliases => 'ip6-loopback';
    'ip6-localnet':
      ip => 'fe00::0';
    'ip6-mcastprefix':
      ip => 'ff00::0';
    'ip6-allnodes':
      ip => 'ff02::1';
    'ip6-allrouters':
      ip => 'ff02::2';
    'ip6-allhosts':
      ip => 'ff02::3';
    'debian.example.com':
      ensure => absent;
  }

  host {
    $::hostname:
      ensure => absent;
    $::fqdn:
      ip           => $localip,
      host_aliases => $::hostname;
  }
}
