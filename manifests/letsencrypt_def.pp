# Manager Letsencrypt key
define sys::letsencrypt_def (
  String $acme_path,
  String $http_server = 'nginx',
  Boolean $pre_hook_http_server = true,
  Enum['standalone', 'webroot'] $renew = 'webroot',
  String $renew_deploy_hook = 'systemctl reload nginx',
  $cert_domains = undef,
) {

  require letsencrypt

  if $renew == 'webroot' {
    ensure_resource('sys::letsencrypt_acme', $acme_path, {})
    $manage_cron = false
  } else {
    $manage_cron = true
  }

  if ! $cert_domains {
    $domains = [$name]
  } else {
    $domains = $cert_domains
  }

  if $pre_hook_http_server {

    # le pre hook servant à la fois à la création et au renouvellement :
    # le serveur sera stoppé seulement si le certificat ne correspond plus (plus moyen de renouveler à coup sûr via webroot)
    $live_path_certname = regsubst($name, '^\*\.', '')
    $live_path = "/etc/letsencrypt/live/${live_path_certname}/cert.pem"
    $verify_domains = join(unique($domains), '\' \'')

    letsencrypt::certonly { $name:
      domains            => $domains,
      additional_args    => ['--keep', '--non-interactive'],
      plugin             => 'standalone',
      pre_hook_commands  => "/usr/local/sbin/letsencrypt-domain-validation ${live_path} '${verify_domains}' || /usr/bin/systemctl stop ${http_server}",
      post_hook_commands => "/usr/bin/systemctl is-active ${http_server} && /usr/bin/systemctl reload ${http_server}",
      manage_cron        => $manage_cron,
    }
  } else {
    letsencrypt::certonly { $name:
      domains              => $domains,
      additional_args      => ['--keep', '--non-interactive'],
      plugin               => 'standalone',
      post_hook_commands   => "/usr/bin/systemctl is-active ${http_server} && /usr/bin/systemctl reload ${http_server}",
      manage_cron          => $manage_cron,
      suppress_cron_output => true
    }
  }

  if $renew == 'webroot' {
    $execution_environment = [ "VENV_PATH=${letsencrypt::venv_path}" ]
    $_domains = join($domains, '\' -d \'')
    $cron_cmd = "certbot --text --agree-tos --non-interactive certonly --rsa-key-size 4096 -a webroot --cert-name '${name}' --webroot-path ${acme_path} -d '${_domains}' --keep --non-interactive --keep-until-expiring --deploy-hook '${renew_deploy_hook}'"

    file { "${letsencrypt::cron_scripts_path}/renew-${title}.sh":
      ensure  => file,
      mode    => '0755',
      owner   => 'root',
      group   => $letsencrypt::cron_owner_group,
      content => template('letsencrypt/renew-script.sh.erb'),
    }

    cron { "letsencrypt renew cron ${title}":
      ensure   => present,
      command  => "\"${letsencrypt::cron_scripts_path}/renew-${name}.sh\" > /dev/null 2>&1",
      user     => 'root',
      hour     => fqdn_rand(24, $name),
      minute   => fqdn_rand(60, fqdn_rand_string(10, $name)),
      monthday => '*',
    }
  }

}
