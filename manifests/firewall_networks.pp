# Firewall
define sys::firewall_networks (
  Integer $baserulenumber = 200,
  Array[Hash] $networks = [],
) {

  $networks.each |Integer $index, Hash[
      Enum['source_name', 'service_name', 'rule'],
      Variant[String, Hash[String, Variant[String, Array[Variant[String, Integer]]]]]
    ] $network| {
    $id = ($baserulenumber+$index)
    if $network['rule']['destination'] != undef and $network['rule']['destination'] != '' {
      if $network['rule']['jump'] == 'DNAT' {
          $rulename = "${id} Redirect ${network['service_name']} to (${network['source_name']}) ${network['rule']['todest']}"
      } else {
          $rulename = "${id} Allow ${network['service_name']} to ${network['source_name']}"
      }
    } else {
      $rulename = "${id} Allow ${network['service_name']} from ${network['source_name']}"
    }
    firewall { $rulename:
      *   => $network['rule'],
    }
  }

}
