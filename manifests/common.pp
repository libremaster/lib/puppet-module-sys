# Common class
class sys::common (
  Boolean $postfix_out = true,
  Boolean $suspended = false,
  Array[Hash] $postfix_out_dkim_keys = [],
  Array[Hash] $postfix_out_smtp_generic_maps = [],
  Array[Hash] $postfix_out_sender_canonical_maps = [],
  Array[Hash] $postfix_out_recipient_canonical_maps = [],
  Array[Hash] $postfix_out_transport_maps = [],
  Array[Hash] $postfix_out_virtual_alias_maps = [],
  Array[Hash] $postfix_out_sender_dependent_relayhost_maps = [],
  Array[Hash] $postfix_out_sasl_maps = [],
) {
  stage {'hosts':}
  stage {'root':}
  stage {'gnupg':}
  stage {'apt':}
  stage {'update':}
  stage {'packages':}
  stage {'init':}
  Stage['hosts'] -> Stage['root'] -> Stage['gnupg'] -> Stage['apt'] -> Stage['update'] -> Stage['packages'] -> Stage['init'] -> Stage['main']

  class {'sys::hosts': stage => 'hosts'}
  class {'sys::user_root': stage => 'root' }
  class {'sys::ssh_keys': }
  class {'sys::gnupg' : stage => 'gnupg' }
  class {'sys::apt' : stage => 'apt' }
  class {'sys::update' : stage => 'update' }
  class {'sys::packages' : stage => 'packages' }
  class {'sys::firewall': stage => 'init', suspended => $suspended }

  if $postfix_out {
    class {'sys::postfix_out':
      stage                           => 'init',
      dkim_keys                       => $postfix_out_dkim_keys,
      smtp_generic_maps               => $postfix_out_smtp_generic_maps,
      sender_canonical_maps           => $postfix_out_sender_canonical_maps,
      recipient_canonical_maps        => $postfix_out_recipient_canonical_maps,
      transport_maps                  => $postfix_out_transport_maps,
      virtual_alias_maps              => $postfix_out_virtual_alias_maps,
      sender_dependent_relayhost_maps => $postfix_out_sender_dependent_relayhost_maps,
      sasl_maps                       => $postfix_out_sasl_maps
    }
  }

  class {'sys::sysctl': }
  class {'ssh': }
  class {'sys::system': }
  class {'sys::fail2ban': }
}
