# Class: SYS / SYSTEM OS
class sys::system (
  String $localtime = 'Europe/Paris',
  String $kblayout = 'fr',
  String $kbvariant = 'oss'
) {

  # Nettoyage des fichiers temporaires non accédés
  package{'tmpreaper':}
  file {
    '/etc/tmpreaper.conf':
      source  => "puppet:///modules/${module_name}/system/etc/tmpreaper.conf",
      mode    => '0644',
      owner   => 'root',
      group   => 'root',
      require => Package['tmpreaper'];
  }

  # Librairie de base pour les scripts
  ensure_packages(['uuid-runtime', 'bsd-mailx'])
  file {
    '/usr/local/bin/lib.sh':
      source  => "puppet:///modules/${module_name}/system/usr/local/bin/lib.sh",
      mode    => '0755',
      owner   => 'root',
      group   => 'root',
      require => Package['bsd-mailx'];
    '/usr/local/bin/onlyone.sh':
      source => "puppet:///modules/${module_name}/system/usr/local/bin/onlyone.sh",
      mode   => '0755',
      owner  => 'root',
      group  => 'root';
  }

  # Locales
  package{'locales-all':}
  file {
    '/etc/profile.d/01-locale':
      content => ': "${LANG:=en_US.UTF-8}"; export LANG',
      mode    => '0644',
      owner   => 'root',
      group   => 'root';
    '/etc/localtime':
      ensure => link,
      target => "/usr/share/zoneinfo/${localtime}";
  }

  # console
  ensure_packages(['keyboard-configuration', 'console-setup'])
  file {
    '/etc/default/keyboard':
      content => template("${module_name}/system/etc/default/keyboard"),
      mode    => '0644',
      owner   => 'root',
      group   => 'root',
      require => Package['keyboard-configuration'],
      notify  => Exec['keyboard_reload'];
    '/etc/default/console-setup':
      source  => "puppet:///modules/${module_name}/system/etc/default/console-setup",
      mode    => '0644',
      owner   => 'root',
      group   => 'root',
      require => Package['console-setup'];
  }
  exec {
    'keyboard_reload':
      command     => '/usr/sbin/service keyboard-setup restart',
      refreshonly => true,
  }

}
