# Manager SSH keys
class sys::ssh_keys (
  Hash $keys = undef,
) {

  if $keys != undef {
    create_resources('ssh_authorized_key', $keys)
  }

}
