# Packages principaux et utiles
class sys::packages {

  # PACKAGES
  # outils système
  ensure_packages(['htop', 'screen', 'rsync', 'strace', 'lsof', 'parted', 'sudo', 'sysstat', 'gdisk', 'lockfile-progs', 'at'])

  # outils réseaux
  ensure_packages(['ethtool', 'traceroute', 'tcpdump', 'netcat'])

  # outils web
  ensure_packages(['curl', 'wget', 'lynx'])

  # outils divers
  ensure_packages(['man-db'])

  # archiveurs
  ensure_packages(['bzip2', 'unzip'])

  # éditeurs, explorateurs
  ensure_packages(['vim', 'mc', 'liquidprompt'])
  file {
    '/etc/vim/vimrc.local':
      content => 'vmap "+y :!xclip -f -sel clip',
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      require => Package['vim'];
  }

  # packages à ne pas mettre
  package {'mlocate': ensure => absent }

}
