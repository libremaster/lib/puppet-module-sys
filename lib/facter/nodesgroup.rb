require 'facter'

# nom hote de la forme abc123def -> récupère abc
Facter.add(:groupname) do
  setcode do
    Facter.value(:hostname)[/^([^\d]*)(\d*)(.*)$/,1]
  end
end

# nom hote de la forme abc123def -> récupère 123
Facter.add(:groupversion) do
  setcode do
    Facter.value(:hostname)[/^([^\d]*)(\d*)(.*)$/,2]
  end
end

# nom hote de la forme abc123def -> récupère def
Facter.add(:clientid) do
  setcode do
    Facter.value(:hostname)[/^([^\d]*)(\d*)(.*)$/,3]
  end
end
