#!/bin/bash

# Valeur par défaut
debug=0

if [ "$1" == "" ]; then
	exit 1
fi

if [ "$2" == "debug" ]; then
	debug=1
	echo "begin : `date +%c`"
fi

command=`echo -n "$1" | md5sum | cut -f1 -d' '`
lck="/tmp/`basename $0`-$command"

if [ "$debug" -eq 1 ]; then
	echo $lck
fi

function end {
	if [ "$debug" -eq 1 ]; then
		echo "end : `date +%c`"
	fi
}

function cleanup {
	echo "Can't lock file... quitting."
	end
	exit 1
}

function cleanupall {
	echo "Caught Signal ... cleaning up."
	kill "${lckpid}"
	lockfile-remove $lck
	echo "Done cleanup ... quitting."
	end
	exit 1
}

lockfile-create -r 1 $lck || cleanup
lockfile-touch $lck &
# Save the PID of the lockfile-touch process
lckpid="$!"

trap cleanupall HUP INT QUIT ABRT KILL

$1

kill "${lckpid}"
lockfile-remove $lck

end

