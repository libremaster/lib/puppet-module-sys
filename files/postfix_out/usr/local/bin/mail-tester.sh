#!/bin/bash

# parameters
debug=0
txt=0
fromname=""

# default txt email template
read -r -d '' EMAIL_MSG_TXT <<- EOM
	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur nec nulla felis. Cras dictum erat ut libero euismod ullamcorper. Nulla ut suscipit nisi. Vestibulum at risus ut nisi dignissim cursus. Suspendisse lobortis tempus interdum. Donec eu vestibulum lacus. Nam commodo dui in faucibus pharetra. In id fringilla risus.

	Donec ullamcorper, neque sit amet sagittis lobortis, urna sem bibendum magna, maximus semper ante nunc sed lorem. Sed suscipit quam nibh, ut placerat urna dapibus in. Praesent ac erat vel arcu convallis viverra eu in libero. Duis cursus dapibus euismod. Morbi congue dolor sapien. Donec sagittis sit amet lorem vitae consequat. Donec aliquam risus vel nisi interdum hendrerit. Maecenas sed lectus arcu. Vestibulum eleifend nisl ut elementum faucibus. In vehicula arcu vitae molestie condimentum. Donec non orci turpis. Donec nec convallis mi. Nunc porta quam a ex luctus imperdiet. Nullam ut dictum sapien. Sed erat mi, suscipit non enim a, lobortis sodales magna. Donec placerat tellus at efficitur condimentum. 

	-- 
	Mail tester
EOM

# default email template
email_boundary=$(uuidgen -t)
read -r -d '' EMAIL_MSG <<- EOM
	--${email_boundary}
	Content-Type: text/plain; charset="utf-8"

	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur nec nulla felis. Cras dictum erat ut libero euismod ullamcorper. Nulla ut suscipit nisi. Vestibulum at risus ut nisi dignissim cursus. Suspendisse lobortis tempus interdum. Donec eu vestibulum lacus. Nam commodo dui in faucibus pharetra. In id fringilla risus.

	Donec ullamcorper, neque sit amet sagittis lobortis, urna sem bibendum magna, maximus semper ante nunc sed lorem. Sed suscipit quam nibh, ut placerat urna dapibus in. Praesent ac erat vel arcu convallis viverra eu in libero. Duis cursus dapibus euismod. Morbi congue dolor sapien. Donec sagittis sit amet lorem vitae consequat. Donec aliquam risus vel nisi interdum hendrerit. Maecenas sed lectus arcu. Vestibulum eleifend nisl ut elementum faucibus. In vehicula arcu vitae molestie condimentum. Donec non orci turpis. Donec nec convallis mi. Nunc porta quam a ex luctus imperdiet. Nullam ut dictum sapien. Sed erat mi, suscipit non enim a, lobortis sodales magna. Donec placerat tellus at efficitur condimentum. 

	-- 
	Mail tester

	--${email_boundary}
	Content-type: text/html; charset="utf-8"

	<!DOCTYPE html>
	<head>
	<meta charset="UTF-8">
	</head>
	<body>
	<p>
	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur nec nulla felis. Cras dictum erat ut libero euismod ullamcorper. Nulla ut suscipit nisi. Vestibulum at risus ut nisi dignissim cursus. Suspendisse lobortis tempus interdum. Donec eu vestibulum lacus. Nam commodo dui in faucibus pharetra. In id fringilla risus.
	</p>
	<p>
	Donec ullamcorper, neque sit amet sagittis lobortis, urna sem bibendum magna, maximus semper ante nunc sed lorem. Sed suscipit quam nibh, ut placerat urna dapibus in. Praesent ac erat vel arcu convallis viverra eu in libero. Duis cursus dapibus euismod. Morbi congue dolor sapien. Donec sagittis sit amet lorem vitae consequat. Donec aliquam risus vel nisi interdum hendrerit. Maecenas sed lectus arcu. Vestibulum eleifend nisl ut elementum faucibus. In vehicula arcu vitae molestie condimentum. Donec non orci turpis. Donec nec convallis mi. Nunc porta quam a ex luctus imperdiet. Nullam ut dictum sapien. Sed erat mi, suscipit non enim a, lobortis sodales magna. Donec placerat tellus at efficitur condimentum. 
	</p>
	<p>
	<strong>Mail tester</strong>
	</p>
	</body>
	</html>

	--${email_boundary}--
EOM

# read args
usage () { echo "script usage: $(basename $0) [-x] -m email -r fromemail [-f fromname]"; exit 1; }

while getopts "xm:r:f:t" option; do
	case "${option}" in
		x)
			debug=1
			;;
		m)
			email=${OPTARG}
			;;
		r)
			fromemail=${OPTARG}
			;;
		f)
			fromname=${OPTARG}
			;;
		t)
			txt=1
			;;
		?)
			usage
			;;
	esac
done

# debug
if [ $debug -eq 1 ]; then
	echo "====================== DEBUG ======================"
	echo "to: $email"
	echo "fromemail: $fromemail"
	echo "fromname: $fromname"
	echo "txt: $txt"
fi

if [ -z $email ]; then
	usage
fi

if [ -z $fromemail ]; then
	usage
fi

if [ $debug -eq 1 ]; then
	echo "====================== EMAIL ======================"
	echo 
	if [ $txt -eq 1 ]; then
		echo $EMAIL_MSG_TXT
	else
		echo $EMAIL_MSG
	fi
fi

if [ $txt -eq 1 ]; then
	echo "${EMAIL_MSG_TXT}" | bsd-mailx -r "${fromname}<${fromemail}>" -s "=?utf-8?B?$(base64 --wrap=0 <<< "Test")?=" $email
else
	echo "${EMAIL_MSG}" | bsd-mailx -r "${fromname}<${fromemail}>" -a "Content-Type: multipart/alternative; boundary=${email_boundary}" -s "=?utf-8?B?$(base64 --wrap=0 <<< "Test")?=" $email
fi
